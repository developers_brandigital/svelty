@extends('_layouts.master')
@section('body')
    <div class="wrapperFull" id="move">
        <!-- Swiper -->
        <section class="container-fluid internaMain shadow-sm position-relative overflow-hidden">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="w-100 swiper-container position-relative pt-0">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide position-relative">
                                    <div class="wrapper-play d-flex w-100 h-100 justify-content-center align-items-center">
                                        <div class="cont-img overflow-hidden d-flex align-items-center justify-content-center order-2 order-md-1">
                                            <img src="assets/img/move/hero-dk.png" alt="Banner Svelty" class="d-none d-md-block" />
                                            <img src="assets/img/move/hero-mb.png" alt="Banner Svelty" class="d-md-none hero-mb" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wrapper info -->
                        <div class="w-100 h-100 position relative hero-cont">
                            <div class="h-100 d-flex align-items-center justify-content-center hero_bg">
                                <div class="d-md-none hero-cont_bg">
                                    <img src="assets/img/bg-hero-mb.png" alt="">
                                </div>
                                <div class="container pb-4 pb-md-0 hero-cont_info">
                                    <div class="row justify-content-center">
                                        <div class="col-md-11 col-lg-9 col-xl-8">
                                            <div class="row d-md-flex justify-content-md-end">
                                                <div class="col-md-6">
                                                    <div class="text-center">
                                                        <div class="mb-md-2 hero-cont_img">
                                                            <img src="assets/img/logo-move.png" alt="Logo Move+" class="img-fluid">
                                                        </div>
                                                        <h1 class="mb-0"><span class="h2-lg">ES UNA COMBINACIÓN </h1>
                                                        <h1 class="mb-md-2">EXCLUSIVA</h1>
                                                        <h5>DE VITAMINAS, MINERALES Y PROTEÍNAS<br/> ESPECIALMENTE DISEÑADA PARA<br/> AYUDARTE A MANTENER UNA VIDA ACTIVA.</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pagination -->
                        <div class="swiper-pagination position-relative w-100 my-2"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Move -->
        <section class="py-4 py-md-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-11 col-lg-10 col-xl-8">
                        <div class="row">
                            <div class="col-md-4 mb-3 mb-md-0 text-center item-move">
                                <div class="mb-2">
                                    <img src="assets/img/move/vitaminaB.png" alt="Vitamina B" class="img-fluid">
                                </div>
                                <div>
                                    <p>Las proteínas de alta calidad<br/>
                                        son importantes para la <strong>salud<br/>
                                            de los músculos.</strong>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3 mb-md-0 text-center item-move">
                                <div class="mb-2">
                                    <img src="assets/img/move/calcio.png" alt="Calcio" class="img-fluid">
                                </div>
                                <div>
                                    <p>Calcio Plus es una combinación de<br class="d-md-none"/>
                                        <strong>Calcio, Zinc, Fósforo y 3 vitaminas<br class="d-md-none"/></strong>
                                        (C, D3 Y K1), que participan en el metabolismo óseo, permitiendo una<br class="d-md-none"/> mejor absorción y fijación del calcio.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 text-center item-move">
                                <div class="mb-2">
                                    <img src="assets/img/move/proteinas.png" alt="Proteinas" class="img-fluid">
                                </div>
                                <div>
                                    <p>Las vitaminas del complejo B<br/>
                                        son importantes para un normal<br/>
                                        <strong>metabolismo energético.</strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
