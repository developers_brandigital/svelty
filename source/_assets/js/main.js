//---------------------------------Generales
function anclaanimada(){
  //redirigir anclas de forma animada
  $(".scroll").on('click', function(e){
    e.preventDefault();
    var codigo = $(this).attr("href");
    $("html,body").animate({scrollTop: $(codigo).offset().top}, 500);
  });
}

function togglemenu(){
  if ($(window).width() < 1200) {
    $('header .navbar-toggler').on('click', function () {
      $('body').toggleClass('header-active');
    });
  }
}
function sticky() {
  if ($(window).width() < 767) {
    if ($('header').hasClass('sticky')) {
      var header = $('header.sticky');
      var header_offset = header.offset();
      $(window).on('scroll', function () {
        if ($(window).scrollTop() > header_offset.top) {
          header.addClass('selected');
        } else {
          header.removeClass('selected');

        }
      });
    }
  }
}
function btnToTop(){
  //btn to top
  $(window).on('scroll resize', function () {
    var windowTop, windowBottom, heightfoo, stoptop, stopbottom, btnIni;

    //dimensiones footer & pag
    windowTop= $(document).scrollTop();
    heightfoo= $('footer').height();
    windowBottom = windowTop + window.innerHeight;
    windowBottomZone= windowBottom + 50;
    stoptop= $('footer').offset().top;
    fh= $('footer').height();
    fhFin= fh - 250;
    stopbottom= stoptop + $('footer').height();
    stopbottom2 = stoptop + fhFin;

    //Btn to top
    if($(window).width() >= 320){
      btnIni= $('.ico-top');
      btnIni.addClass('visible');

      if(stopbottom < windowBottomZone){
        btnIni.css('margin-bottom',  heightfoo + 45);
        btnIni.addClass("bottom");
      }else{
        btnIni.css('margin-bottom',  0);
        btnIni.removeClass("bottom");
      }

      if(windowTop == 0){
        btnIni.removeClass('visible');
      }
    }
  });
}

function search(){
    //Abrir wrapper search Desktop
    if($(window).width() > 767){
      $('.wrapper-search a.open').on('click', function(e){
        e.preventDefault();
        $(this).prev('div').find('input').val('');
        $(this).prev('div').find('.close-i').removeClass('active');
        $(this).prev('div').find('.search').removeClass('active');

        if($(this).parent('.wrapper-search').hasClass('selected')){
          $(this).parent('.wrapper-search').removeClass('selected');
          $(this).prev('div').find('input').val('');
        }else{
          $(this).parent('.wrapper-search').addClass('selected');
          $(this).prev('div').find('input').focus();
        }
      });
    }

    //Borrar busqueda
     $('.wrapper-search > div > .close-i').on('click', function(e){
        e.preventDefault();
        $(this).prev('input').val('');
        $(this).prev('input').focus();
        $(this).removeClass('active');
        $(this).siblings('.search').removeClass('active');
    });

    //Aparecer close al escribir
    $('.wrapper-search > div > input').on('keyup', function () {
        if($(this).val().length != ""){
            $(this).next('.close-i').addClass('active');
            $(this).siblings('.search').addClass('active');
        }else{
            $(this).next('.close-i').removeClass('active');
            $(this).siblings('.search').removeClass('active');
        }
    });

    //Limpiar busqueda al abrir el toggle en Mobile
    $('.navbar-toggler').on('click', function (){
      $('.wrapper-search').find('input').val('');
      $('.wrapper-search .close-i').removeClass('active');
      $('.wrapper-search .search').removeClass('active');
    });
}
function menu(){
    var id_= $('.wrapperFull').attr('id');
    var id= id_.replace('#', '');

    $('.navbar-nav > .nav-item > .nav-link').parent().removeClass('active');
    $('.navbar-nav > .nav-item > .nav-link[data-id="' + id + '"]').parent().addClass('active');

    //productos
    if($('.wrapperFull').attr('id') == "productos"){
      console.log('entro');
      var id_prod= $('.wrapperFull').attr('data-prod');
      $('.navbar-nav > .nav-item > .menu-collapse >.card > ul > li > a').removeClass('active');
      $('.navbar-nav > .nav-item > .menu-collapse >.card > ul > li > a[data-id="' + id_prod + '"]').addClass('active');
    }
}
//---------------------------------Slides
function sliderMain(){
  sliderSwiper('.SliderMain', 'horizontal', 1, 0);
}
function internaMain() {
  sliderSwiper('.internaMain', 'horizontal', 1, 0);
}
function carouselCard() {
  sliderSwiper('.carouselCard', 'horizontal', 1, 0);
}
function simpleCarousel() {
  sliderSwiper('.simpleCarousel', 'horizontal', 1, 10);
}

//---------------------------------Template de Slides func (class del div., horiz o vert., slidesperview, slide to click slide, breackpoints)
function sliderSwiper(DivSwiper, direction, spv, space){
  var swiper = new Swiper(DivSwiper + '  .swiper-container', {
    direction: direction,
    slidesPerView: spv,
    spaceBetween: space,
    observer: true,
    observeParents: true,
    navigation: {
      nextEl: DivSwiper + ' .swiper-button-next',
      prevEl: DivSwiper + ' .swiper-button-prev',
    },
    grabCursor: true,
    pagination: {
      el: DivSwiper + ' .swiper-pagination',
      clickable: true
    }
  });

  if($(DivSwiper + " .swiper-slide").length == 1) {
    $(DivSwiper + " .swiper-pagination").addClass( "disabled" );
  }
}
//---------------------------------FormEmail

/*jQuery Global Functions*/
$(document).ready(function(){
  anclaanimada();
  togglemenu();
  btnToTop();
  sliderMain();
  internaMain();
  carouselCard();
  simpleCarousel();
  search();
  menu();
  sticky();
});
