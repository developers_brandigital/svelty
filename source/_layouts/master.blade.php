<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href="/svelty/">
    <title>{{ $page->title }}</title>
    <link rel="shortcut icon" type="image/ico" href="assets/img/fav.ico">
    <link rel="stylesheet" type="text/css" href="assets/plugins/swiper/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="{{ ltrim(mix('css/main.css', 'assets/build'), '/') }}">
    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,300;0,400;0,600;0,700;0,900;1,400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Love+Ya+Like+A+Sister&display=swap" rel="stylesheet">
    <!-- Font awesome CDN-->
    <script src="https://kit.fontawesome.com/d241728d20.js" crossorigin="anonymous"></script>
</head>
<body class="bg-texture">
<header class="w-100 bg-white sticky" id="inicio">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-transparent py-2 py-lg-4 px-0">
            <a class="navbar-brand px-1 navbar_logo" href="{{$page->baseUrl}}">
                <div>
                    <img src="assets/img/logo-svelty.png" alt="Nestle Svelty" class="img-fluid" />
                </div>
            </a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <div class="w-100 d-flex justify-content-center align-items-center flex-column-reverse flex-lg-row my-4 my-lg-0 navbar-cont">
                    <ul class="navbar-nav align-items-center ml-0">
                        <li class="nav-item">
                            <a class="nav-link px-3 py-2 text-uppercase" data-id="home" href="{{$page->baseUrl}}">Inicio</a>
                        </li>
                        <li class="nav-item submenu">
                            <a class="nav-link px-3 py-2 text-uppercase submenu_link" data-id="productos" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Productos</a>

                            <div class="collapse menu-collapse w-100" id="collapseExample">
                                <div class="card card-body bg-transparent mb-3 mb-md-5 mt-md-4 py-lg-5">
                                    <ul class="p-0 m-0 w-100 text-center">
                                        <li class="mb-3"><a data-id="prod-1" href="productos/svelty-colageno">Svelty Colágeno Move+</a></li>
                                        <li class="mb-3"><a data-id="prod-2" href="productos/svelty-descremada">Svelty Descremada con Move+</a></li>
                                        <li class="mb-3"><a data-id="prod-3" href="productos/svelty-sinLactosa">Svelty Sin Lactosa con Move+</a></li>
                                        <li class="mb-3"><a data-id="prod-4" href="productos/svelty-calcilock">Svelty Calcilock (Huesos)</a></li>
                                        <li class="mb-3"><a data-id="prod-5" href="productos/svelty-sinLactosa-liquida">Svelty Descremada Sin Lactosa (Líquida)</a></li>
                                        <li><a data-id="prod-6" href="productos/svelty-cacao">Svelty Cacao Sin Lactosa (Líquida)</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-3 py-2 text-uppercase" data-id="move" href="move">Move+</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-3 py-2 text-uppercase" data-id="tips" href="tips">Tips</a>
                        </li>
                    </ul>
                    <!-- <form class="wrapper-search form-inline my-2 my-lg-0 mr-0 mr-xl-0 ml-0 ml-xl-2 pt-3 pt-lg-0">
                        <div class="rounded-pill overflow-hidden">
                            <input class="form-control rounded-pill w-100" type="search" placeholder="Search" aria-label="Search">
                            <p class="fas fa-times mb-0 close-i"></p>
                            <button class="btn btn-link search ico-glass text-white p-0 m-0 d-block d-lg-none" type="submit">
                                <svg viewBox="0 0 79.94 80.04"><path d="M18.81,69.22a29.91,29.91,0,0,0,39.3,2.7L80,96.13A5.74,5.74,0,1,0,88.11,88L63.91,66a30,30,0,1,0-45.1,3.2Zm5.6-36.9a22,22,0,1,1,0,31.1A22,22,0,0,1,24.41,32.32Z" transform="translate(-10.04 -17.95)"/></svg>
                            </button>
                        </div>
                        <a class="btn btn-link open ico-glass mx-2 p-0">
                            <svg viewBox="0 0 79.94 80.04"><path d="M18.81,69.22a29.91,29.91,0,0,0,39.3,2.7L80,96.13A5.74,5.74,0,1,0,88.11,88L63.91,66a30,30,0,1,0-45.1,3.2Zm5.6-36.9a22,22,0,1,1,0,31.1A22,22,0,0,1,24.41,32.32Z" transform="translate(-10.04 -17.95)"/></svg>
                        </a>
                    </form> -->
                </div>
            </div>
        </nav>
    </div>
</header>
<div class="ico-top position-fixed">
    <a href="#inicio" class="text-primary rounded-circle scroll">
        <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="arrow-circle-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-arrow-circle-up fa-w-16 fa-2x"><path fill="currentColor" d="M256 504c137 0 248-111 248-248S393 8 256 8 8 119 8 256s111 248 248 248zM40 256c0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216 0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216zm92.5-12.5l115-115.1c4.7-4.7 12.3-4.7 17 0l115 115.1c4.7 4.7 4.7 12.3 0 17l-6.9 6.9c-4.7 4.7-12.5 4.7-17.1-.2L273 181.7V372c0 6.6-5.4 12-12 12h-10c-6.6 0-12-5.4-12-12V181.7l-82.5 85.6c-4.7 4.8-12.4 4.9-17.1.2l-6.9-6.9c-4.7-4.8-4.7-12.4 0-17.1z" class=""></path></svg>
    </a>
</div>
@yield('body')

<footer class="bg-white py-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 mb-3 mb-md-0 d-flex justify-content-center justify-content-md-start align-items-center">
                <div class="row no-gutters nestle-brand">
                    <a href="{{$page->baseUrl}}"><img src="assets/img/logo-svelty.png" alt="Nestle Svelty" class="img-fluid px-1"></a>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row no-gutters justify-content-center align-items-center flex-column">
                    <p class="mb-3 text-primary text-center">Síguenos en nuestras redes sociales:</p>
                    <div class="wrapper-social d-flex justify-content-center align-items-center">
                        <a href="https://www.facebook.com/SveltyNestleChile" target="_blank" class="ico-fb"></a>
                        <a href="https://www.instagram.com/svelty_cl/" target="_blank" class="ico-ig mx-3"></a>
                        <a href="https://www.youtube.com/channel/UCniZkdYdMiTBsXtr3vhAmOA" target="_blank" class="ico-yt"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-none d-lg-block"></div>
        </div>
    </div>
</footer>

<!-- modal -->
@foreach ($page->modales as $modal)
<div class="modal fade modalTabla" id="{{ $modal['id'] }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal-1" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content bg-transparent border-0">
            <div class="modal-header border-0 mt-4 mt-md-0">
                <h4 class="modal-title text-center w-100 font-weight-bold text-uppercase" id="modal-1">Información Nutricional</h4>
            </div>
            @isset($modal['descripcion'])
                <div class="text-center">
                    <p>{!! $modal['descripcion'] !!}</p>
                </div>
            @endisset
            <div class="modal-body">
                <div class="row justify-content-center no-gutters">
                    <div class="col-md-10 col-xl-5">
                        <div class="col-md-12 tab-nutri mb-3 mb-md-4">{!! $modal['tabla'] !!}</div>
                        @isset($modal['leyenda'])
                            <div class=text-center>{!! $modal['leyenda'] !!}</div>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach


<!--bootstrap CDN-->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!--Swiper CDN-->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script type="text/javascript" src="{{ ltrim(mix('js/main.js', 'assets/build'), '/') }}"></script>
</body>

</html>
