@extends('_layouts.master')
@section('body')
    <div class="wrapperFull" id="tips">
        <!-- Swiper -->
        <section class="container-fluid internaMain shadow-sm position-relative overflow-hidden">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="w-100 swiper-container position-relative pt-0">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide position-relative">
                                    <div class="wrapper-play d-flex w-100 h-100 justify-content-center align-items-center">
                                        <div class="cont-img overflow-hidden d-flex align-items-center justify-content-center order-2 order-md-1">
                                            <img src="assets/img/tips/hero-dk.png" alt="Banner Svelty" class="d-none d-md-block" />
                                            <img src="assets/img/tips/hero-mb.png" alt="Banner Svelty" class="d-md-none hero-mb" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wrapper info -->
                        <div class="w-100 h-100 position relative hero-cont">
                            <div class="h-100 d-flex align-items-center justify-content-center hero_bg">
                                <div class="d-md-none hero-cont_bg">
                                    <img src="assets/img/bg-hero-mb.png" alt="">
                                </div>
                                <div class="container pb-4 pb-md-0 hero-cont_info">
                                    <div class="row justify-content-center">
                                        <div class="col-md-11 col-lg-9 col-xl-8">
                                            <div class="row d-md-flex justify-content-md-end">
                                                <div class="col-md-6">
                                                    <div class="text-center pl-md-5">
                                                        <h1 class="mb-0"><span class="h2-lg">TIPS</h1>
                                                        <h1 class="">INTERESANTES</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pagination -->
                        <div class="swiper-pagination position-relative w-100 my-2"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Tips -->
        <section class="pt-5 pb-4 py-sm-3 py-md-5 bg-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-11 col-sm-9 col-lg-10 card-tips">
                        <div class="row">
                            <div class="col-md-6 px-lg-4 px-xl-5 mb-3 mb-md-0">
                                <div class="px-3 px-md-0">
                                    <img src="assets/img/tips/tips-1.png" alt="" class="img-fluid">
                                </div>
                                <div class="text-center mt-3">
                                    <h2>LA IMPORTANCIA DE LA LECHE</h2>
                                    <p class="px-2 px-md-3 px-lg-4">La leche, al igual que otros productos lácteos, es considerada un <strong>alimento muy completo,</strong> debido a que aporta gran variedad de nutrientes como <strong>carbohidratos, proteínas, minerales y vitaminas.</strong> Según las Guías Alimentarias para la población Chilena, la recomendación es consumir <strong>3 veces al día lácteos</strong> bajos en grasa y azúcar.</p>
                                </div>
                            </div>
                            <div class="col-md-6 px-lg-4 px-xl-5">
                                <div class="px-3 px-md-0">
                                    <img src="assets/img/tips/tips-2.png" alt="" class="img-fluid">
                                </div>
                                <div class="text-center mt-3">
                                    <h2>SVELTY MOVE+</h2>
                                    <p class="px-2 px-md-3 px-lg-4">Todas nuestras variedades de SVELTY MOVE+, <strong>aportan el 100% del calcio y el 98% de la vitamina C</strong> recomendados a diario, en tan sólo dos vasos. De la misma forma, <strong>entregan proteínas</strong> de buena calidad que son importantes para la salud de los músculos.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-11 col-sm-9 col-lg-10 card-tips">
                        <div class="row">
                            <div class="col-md-6 px-lg-4 px-xl-5 mb-3 mb-md-0">
                                <div class="px-3 px-md-0">
                                    <img src="assets/img/tips/tips-3.png" alt="" class="img-fluid">
                                </div>
                                <div class="text-center mt-3 px-lg-3">
                                    <h2>DISFRUTA TU SVELTY</h2>
                                    <p class="px-2 px-md-3 px-lg-4">Puedes consumir tu leche SVELTY <strong>en el momento del día que prefieras:</strong> en el desayuno, entre comidas, a la hora del té o a la noche antes de dormir. Además, puedes <strong>acompañarla con algo rico</strong> para comer, elige unas tostadas, frutos secos, cereales, frutas... ¡lo que más te guste!</p>
                                </div>
                            </div>
                            <div class="col-md-6 px-lg-4 px-xl-5">
                                <div class="px-3 px-md-0">
                                    <img src="assets/img/tips/tips-4.png" alt="" class="img-fluid">
                                </div>
                                <div class="text-center mt-3 px-lg-4">
                                    <h2>MANTENERTE EN MOVIMIENTO</h2>
                                    <p class="px-2 px-md-3 px-lg-4">La actividad física no sólo puede prevenir o reducir el avance de enfermedades, sino que <strong>contribuye favorablemente a la salud ósea, las funciones cardiorrespiratorias y musculares,</strong> y la calidad de vida de las personas, tanto en el bienestar físico como mental. La Organización Mundial de la Salud recomienda a los adultos de 18 a 64 años realizar mínimo 2 horas y media de actividad física moderada a la semana.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
