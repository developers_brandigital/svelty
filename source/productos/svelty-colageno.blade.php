@extends('_layouts.master')
@section('body')
    <div class="wrapperFull" id="productos" data-prod="prod-1">
        <!-- Swiper -->
        <section class="container-fluid internaMain shadow-sm position-relative overflow-hidden">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="w-100 swiper-container position-relative pt-0">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide position-relative">
                                    <div class="wrapper-play d-flex w-100 h-100 justify-content-center align-items-center">
                                        <div class="cont-img overflow-hidden d-flex align-items-center justify-content-center order-2 order-md-1">
                                            <img src="assets/img/productos/hero-dk.png" alt="Banner Svelty" class="d-none d-md-block" />
                                            <img src="assets/img/productos/hero-mb.png" alt="Banner Svelty" class="d-md-none hero-mb" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wrapper info -->
                        <div class="w-100 h-100 position relative hero-cont">
                            <div class="h-100 d-flex align-items-center justify-content-center hero_bg">
                                <div class="d-md-none hero-cont_bg">
                                    <img src="assets/img/bg-hero-mb.png" alt="">
                                </div>
                                <div class="container pb-4 pb-md-0 hero-cont_info">
                                    <div class="row justify-content-center">
                                        <div class="col-md-11 col-lg-9 col-xl-8">
                                            <div class="row d-md-flex justify-content-md-end">
                                                <div class="col-md-6">
                                                    <div class="text-center pl-md-3">
                                                        <h5 class="mb-0">NESTLÉ® PRESENTA</h5>
                                                        <h1 class="mb-0 text-violet"><span class="h2-lg">SVELTY COLÁGENO MOVE+</h1>
                                                        <h2 class="text-violet">LA ÚNICA LECHE CON COLÁGENO</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pagination -->
                        <div class="swiper-pagination position-relative w-100 my-2"></div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Producto -->
        <section class="pt-5 pb-md-0 overflow-hidden">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-9 px-0 produc-content">
                        <div class="row h-100 flex-column justify-content-center">
                            <!-- info  -->
                            <div class="col-lg-6 mb-4 mb-lg-5 text-center px-4 px-md-0">
                                <p class="px-md-4">¿Sabías que <span class="font-weight-bold">a partir de los 45 años</span> el cuerpo de la mujer comienza a perder masa muscular y ósea, generando cansancio y menor rendimiento en la actividad física?</p>
                                <h5 class="h5-sm font-weight-bold text-violet mb-0">POR ESTA RAZÓN LANZAMOS LA NUEVA LECHE</h5>
                                <h3 class="font-lyl text-violet">SVELTY COLÁGENO MOVE+</h3>
                                <p>> Dos vasos aportan <span class="font-weight-bold">5g de colágeno,</span><br/>además del <span class="font-weight-bold">100% del calcio y el 98% <br/>de la vitamina C</span> que se necesita a diario.</p>
                                <p>> Sin lactosa, descremada, <br/>libre de gluten y con MOVE+</p>
                                <!-- botones -->
                                <div class="col-12 boton-move pb-4 pb-lg-0 mt-n3 mt-lg-0 d-none d-md-block">
                                    <div class="text-center">
                                        <button class="btn btn-sm rounded-pill py-2 px-3 btn-outline-primary m-2" data-toggle="modal" data-target="#tabla-svelty-colageno">Información nutricional</button>
                                        <a class="btn btn-sm rounded-pill py-2 px-3 bg-pink text-white m-2" href="https://articulo.mercadolibre.cl/MLC-526318748-leche-en-polvo-svelty-colageno-500g-pack-x2-_JM?quantity=1#position=1&type=item&tracking_id=85b64a3b-2fd8-4949-b207-e3c44d8ef2cc" target="_blank">Compra aquí</a>
                                    </div>
                                </div>
                            </div>
                            <!-- producto svelty -->
                            <div class="col-lg-6 px-0 px-md-3 produc-sv">
                                <div class="text-center">
                                    <img src="assets/img/productos/svelty-colageno.png" alt="" class="img-fluid">
                                </div>
                            </div>

                            <!-- botones -->
                            <div class="col-12 boton-move bg-violet pb-4 pb-lg-0 mt-n3 mt-lg-0 d-md-none">
                                <div class="text-center">
                                    <button class="btn btn-sm rounded-pill py-2 px-3 btn-outline-primary m-2" data-toggle="modal" data-target="#tabla-svelty-colageno">Información nutricional</button>
                                    <a class="btn btn-sm rounded-pill py-2 px-3 bg-pink text-white m-2" href="https://articulo.mercadolibre.cl/MLC-526318748-leche-en-polvo-svelty-colageno-500g-pack-x2-_JM?quantity=1#position=1&type=item&tracking_id=85b64a3b-2fd8-4949-b207-e3c44d8ef2cc" target="_blank">Compra aquí</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-5 bg-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-9">
                        <div class="row align-items-xl-center">
                            <div class="col-md-6 text-center subtitulo order-2 order-md-1">
                                <div class="mb-4">
                                    <h2 class="font-lyl text-violet">DESCUBRE CÓMO PREPARARLA</h2>
                                    <h3 class="h3-sm font-weight-bold text-violet">RECUERDA NO UTILIZAR AGUA HIRVIENDO</h3>
                                </div>
                                <!-- video -->
                                <div class="videoYT">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/zuihiNEICa0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-md-6 text-center px-5 order-1 order-md-2 mb-3 mb-md-0">
                                <p>
                                    <span class="font-weight-bold">La nueva leche Svelty Colágeno Move+,</span> es un producto innovador con colágeno que entrega una combinación de <span class="font-weight-bold">vitaminas, proteínas y minerales</span> pensada para acompañar a las mujeres que buscan mantenerse activas y en movimiento.
                                </p>
                                <p>
                                    Se trata de una leche sin lactosa, con colágeno que cuenta con: <span class="font-weight-bold">vitaminas del complejo B</span>, importantes en el normal funcionamiento del metabolismo energético; <span class="font-weight-bold">calcio plus,</span> una combinación de calcio, zinc, fósforo y 3 vitaminas (C, D3 Y K1) que participan en el metabolismo óseo ayudando a una mejor absorción y fijación del calcio; además de <span class="font-weight-bold">proteínas,</span> importantes para la salud de los músculos.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-9">
                        <div class="row">
                            <div class="col-md-6 text-center mb-3 mb-md-0">
                                <div class="produc-move_img mb-3">
                                    <img src="assets/img/logo-move.png" alt="" class="img-fluid">
                                </div>
                                <h3 class="font-lyl text-violet"><span class="syb_int">¿</span>QUÉ ES MOVE+?</h3>
                                <p>Es una combinación exclusiva de <br/><span class="font-weight-bold">vitaminas, minerales y proteínas</span><br/> especialmente diseñada para<br/>
                                    ayudarte a mantener una vida activa.</p>
                            </div>
                            <div class="col-md-6 text-center">
                                <h2 class="font-lyl text-violet mb-4">DESCRÚBELA</h2>
                                <div class="videoYT">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/mQV9jBxWtVM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <hr class="my-0">
@endsection
