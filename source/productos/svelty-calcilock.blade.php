@extends('_layouts.master')
@section('body')
    <div class="wrapperFull" id="productos" data-prod="prod-4">
        <!-- Swiper -->
        <section class="container-fluid internaMain shadow-sm position-relative overflow-hidden">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="w-100 swiper-container position-relative pt-0">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide position-relative">
                                    <div class="wrapper-play d-flex w-100 h-100 justify-content-center align-items-center">
                                        <div class="cont-img overflow-hidden d-flex align-items-center justify-content-center order-2 order-md-1">
                                            <img src="assets/img/productos/hero-productos-dk.png" alt="Banner Svelty" class="d-none d-md-block" />
                                            <img src="assets/img/productos/hero-productos-mb.png" alt="Banner Svelty" class="d-md-none hero-mb" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- wrapper info -->
                        <div class="w-100 h-100 position relative hero-cont">
                            <div class="h-100 d-flex align-items-center justify-content-center hero_bg">
                                <div class="d-md-none hero-cont_bg">
                                    <img src="assets/img/bg-hero-mb.png" alt="">
                                </div>
                                <div class="container pb-4 pb-md-0 hero-cont_info">
                                    <div class="row justify-content-center">
                                        <div class="col-md-11 col-lg-9 col-xl-8">
                                            <div class="row d-md-flex justify-content-md-end">
                                                <div class="col-md-6">
                                                    <div class="text-center pl-md-3">

                                                        <h1 class="mb-0 text-green"><span class="h2-lg">SVELTY</h1>
                                                        <h1 class="mb-0 text-green"><span class="h2-lg">CALCILOCK</h1>
                                                        <h2 class="h2-lg text-primary">HUESOS</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pagination -->
                        <div class="swiper-pagination position-relative w-100 my-2"></div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Producto -->
        <section class="pt-5 pb-md-0 overflow-hidden">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-9 px-0 produc-content">
                        <div class="row h-100 flex-column justify-content-center">
                            <!-- info  -->
                            <div class="col-lg-6 mb-4 text-center px-4 px-md-0">
                                <p class="px-md-4 font-weight-bold text-green">HUESOS FUERTES PARA QUE<br/> SIGAS MOVIÉNDOTE</p>
                                <p>> Dos vasos aportan el<span class="font-weight-bold"> 100% del calcio</span><br/> que se necesita a diario.</p>
                                <p>> Combinación exclusiva de <span class="font-weight-bold">calcio,<br/>
                                    3 vitaminas y 3 minerales</span> que<br/>
                                    participan en el <span class="font-weight-bold">metabolismo óseo.</span><br/>
                                    Libre de gluten.
                                </p>
                                <!-- botones -->
                                <div class="col-12 boton-move pb-4 pb-lg-0 mt-n3 mt-lg-0 d-none d-md-block">
                                    <div class="text-center">
                                        <button class="btn btn-sm rounded-pill py-2 px-3 btn-outline-primary m-2" data-toggle="modal" data-target="#tabla-svelty-calcilock">Información nutricional</button>
                                        <a class="btn btn-sm rounded-pill py-2 px-3 bg-pink text-white m-2" href="https://articulo.mercadolibre.cl/MLC-546897829-leche-en-polvo-huesos-calcilock-800g-x2-bolsas-_JM?quantity=1" target="_blank">Compra aquí</a>
                                    </div>
                                </div>
                            </div>
                            <!-- producto svelty -->
                            <div class="col-lg-6 px-0 px-md-3 produc-sv">
                                <div class="text-center">
                                    <img src="assets/img/productos/svelty-calcilock.png" alt="" class="img-fluid">
                                </div>
                            </div>

                            <!-- botones -->
                            <div class="col-12 boton-move bg-green pb-4 pb-lg-0 mt-n3 mt-lg-0 d-md-none">
                                <div class="text-center">
                                    <button class="btn btn-sm rounded-pill py-2 px-3 btn-outline-primary m-2" data-toggle="modal" data-target="#tabla-svelty-calcilock">Información nutricional</button>
                                    <a class="btn btn-sm rounded-pill py-2 px-3 bg-pink text-white m-2" href="https://articulo.mercadolibre.cl/MLC-546897829-leche-en-polvo-huesos-calcilock-800g-x2-bolsas-_JM?quantity=1" target="_blank">Compra aquí</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <hr class="my-0">
@endsection
