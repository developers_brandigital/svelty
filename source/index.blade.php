@extends('_layouts.master')
@section('body')
    <div class="wrapperFull" id="home">
        <!-- Swiper -->
        <section class="container-fluid internaMain shadow-sm position-relative overflow-hidden">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="w-100 swiper-container position-relative pt-0">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide position-relative">
                                    <div class="wrapper-play d-flex w-100 h-100 justify-content-center align-items-center">
                                        <div class="cont-img overflow-hidden d-flex align-items-center justify-content-center order-2 order-md-1">
                                            <img src="assets/img/home/hero-dk.png" alt="Banner Svelty" class="d-none d-md-block" />
                                            <img src="assets/img/home/hero-mb.png" alt="Banner Svelty" class="d-md-none hero-mb" />
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="swiper-slide position-relative">
                                    <div class="cont-img overflow-hidden d-flex align-items-center justify-content-center order-2 order-md-1">
                                        <img src="assets/img/home/hero.jpg" alt="hero">
                                    </div>
                                </div> -->
                                <!--<div class="swiper-slide position-relative">
                                    <div class="cont-video embed-responsive embed-responsive-16by9">
                                        <video playsinline="playsinline" autoplay="autoplay" loop="loop" class="embed-responsive-item">
                                            <source src="media/header.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>-->
                            </div>
                        </div>

                        <!-- wrapper info -->
                        <div class="w-100 h-100 position relative hero-cont">
                            <div class="h-100 d-flex align-items-center justify-content-center hero_bg">
                                <div class="d-md-none hero-cont_bg">
                                    <img src="assets/img/bg-hero-mb.png" alt="">
                                </div>
                                <div class="container pb-4 pb-md-0 hero-cont_info">
                                    <div class="row justify-content-center">
                                        <div class="col-md-11 col-lg-9 col-xl-8">
                                            <div class="row d-md-flex justify-content-md-end">
                                                <div class="col-md-6">
                                                    <div class="text-center">
                                                        <h1 class="mb-0"><span class="h2-lg">TOMA LO MEJOR DE</h1>
                                                        <h1 class="h1-lg mb-md-3">LA LECHE</h1>
                                                        <h5>ACOMPAÑA TU CONSUMO<br/> DE LÁCTEOS CON<strong> SVELTY®</strong></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pagination -->
                        <div class="swiper-pagination position-relative w-100 my-2"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5">
            <div class="container position-relative wrapper-new pb-lg-4">
                <div class="row justify-content-center">
                    <div class="col-11 col-lg-9 col-xl-8 actividades">
                        <!-- Actividades -->
                        <article class="d-md-flex align-items-center text-center mb-3 mb-lg-2 py-md-4">
                            <div class="mb-3 mb-md-0 mr-md-4 mr-xl-5">
                                <img src="assets/img/home/fotos-actividades.png" alt="Foto actividades" class="img-fluid" />
                            </div>
                            <div class="logo-move">
                                <img src="assets/img/logo-move.png" alt="Logo Move+" class="img-fluid" />
                            </div>
                        </article>
                        <!-- Productos -->
                        <article class="py-md-4 py-lg-5">
                            <div>
                                <img src="assets/img/home/svelty-productos.png" alt="Productos Svelty" class="img-fluid" />
                            </div>
                        </article>
                    </div>
                </div>
            </div>

            <div class="w-100 fd-madera">
                <img src="assets/img/home/img-madera.jpg" alt="madera" class="img-fluid" />
            </div>
        </section>
    </div>
@endsection
