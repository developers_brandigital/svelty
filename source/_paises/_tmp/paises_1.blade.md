---
country: cl
title: 'DIRECTV GO | Chile'
cta_pruebalo: 'pruébalo gratis'
link_pruebalo: 'https://www.directvgo.com/cl/registrarse/seleccionar-plan'
promo_code: cl
subtitle: '¡Todo en una sola plataforma!'
img_promo: background-ondemand_promo
alt_img_promo: 'DTV en familia'
pr_be_promo: pr_be_cl
alt_pr_be_promo: $59.990
pre_promo: $
pr_promo: '12.792'
heading: '<b>TV en vivo, deportes y más de <br class="d-none d-md-block">10 mil contenidos On Demand</b>'
prefooter: 'Si eres cliente de DIRECTV, <br class="d-block d-md-none">ya tienes <b class="text-primary">DIRECTV GO</b> gratis'
cta_prefooter: 'haz click aquí'
link_cta_prefooter: 'https://www.directvgo.com/cl/iniciar-sesion'
servicios:
    - { img: tv-reprod.png, alt: servicios, h2: 'Pausa y retrocede <br class="d-none d-md-block"><b>En Vivo</b>', h3: '<b class="text-orange2">Vuelve a ver</b> aquel gol <br class="d-none d-md-block"> o momento curioso de <br class="d-none d-md-block">tu programa.' }
    - { img: phone.png, alt: servicios, h2: 'Arma tu propia <br class="d-none d-md-block"><b>Lista GO</b>', h3: '<b class="text-orange2">Selecciona y agrega</b> <br class="d-none d-md-block">tus pelis y series favoritas. <br><span class="font-lightItalic">¡Tú eliges cuándo mirarlas!</span>' }
    - { img: tv-vivo.png, alt: servicios, h2: "¡El nuevo <b class=\"text-uppercase\">zapping</b>!\n", h3: 'Puedes espiar el resto <br class="d-none d-md-block"> de los canales <b class="text-orange2">sin quitar <br class="d-none d-md-block">el que estés viendo.</b>' }
    - { img: all_displays.png, alt: servicios, h2: '¡<b>Suma</b> pantallas!', h3: 'Disfruta desde la <b class="text-orange2">PC</b>, el <b class="text-orange2">celular</b>, <br class="d-none d-md-block">la <b class="text-orange2">tablet</b> o tu <b class="text-primary">TV</b>.' }
packs:
    - { img: foxpremium-rabbit.jpg, alt: 'DTV Pack FOX PREMIUM', title: '<b>¡Estrenos en Series y más Películas!</b>', p: 'Disfruta los estrenos de las series más reconocidas y las mejores películas, que no puedes dejar de ver.' }
    - { destacada: destacada-width, img: hbo-mascotas2.jpg, alt: 'DTV Pack HBO', title: '<b>¡Lo mejor de HBO!</b>', p: 'Series originales, estrenos exclusivos, documentales únicos y más.', cta: '<b>Disfrútalo de cortesía por 6 meses.</b>' }
    - { img: foxsportshd-f1.jpg, alt: 'DTV Pack HBO', title: '<b>24 horas de deporte en HD</b>', p: 'Los grandes torneos europeos y sudamericanos: Copa Sudamericana y Copa Libertadores, deportes extrenos, Fórmula 1, Nascar Rally y WWE.' }
    - { img: cdfhd-futbol.jpg, alt: 'DTV Pack CDF HD', title: '<b>Lo mejor del fútbol chileno</b>', p: 'Todo el campeonato nacional en HD. Transmisiones en vivo y los mejores comentaristas.' }
---
