---
country: uy
title: 'DIRECTV GO | Uruguay'
cta_pruebalo: 'probalo gratis'
link_pruebalo: 'https://www.directvgo.com/uy/registrarse/seleccionar-plan'
promo_code: uy
subtitle: '¡Todo en una sola plataforma!'
img_promo: background-ondemand_promo
alt_img_promo: 'DTV en familia'
pr_be_promo: pr_be_uy
alt_pr_be_promo: $960
pre_promo: $
pr_promo: '768'
heading: '<b>TV en vivo, deportes y más de <br class="d-none d-md-block">10 mil contenidos On Demand</b>'
prefooter: 'Si sos cliente de DIRECTV, <br class="d-block d-md-none">ya tenés <b class="text-primary">DIRECTV GO</b> gratis'
cta_prefooter: 'hacé click aquí'
link_cta_prefooter: 'https://www.directvgo.com/uy/iniciar-sesion'
servicios:
    - { img: tv-reprod.png, alt: servicios, h2: 'Pausá y retrocedé <br class="d-none d-md-block"><b>En Vivo</b>', h3: '<b class="text-orange2">Volvé a ver</b> aquel gol <br class="d-none d-md-block"> o momento curioso de <br class="d-none d-md-block">tu programa.' }
    - { img: phone.png, alt: servicios, h2: 'Armá tu propia <br class="d-none d-md-block"><b>Lista GO</b>', h3: '<b class="text-orange2">Seleccioná y agregá</b> <br class="d-none d-md-block">tus pelis y series favoritas. <br><span class="font-lightItalic">¡Vos elegís cuándo mirarlas!</span>' }
    - { img: tv-vivo.png, alt: servicios, h2: "¡El nuevo <b class=\"text-uppercase\">zapping</b>!\n", h3: 'Podés espiar el resto <br class="d-none d-md-block"> de los canales <b class="text-orange2">sin quitar <br class="d-none d-md-block">el que estés viendo.</b>' }
    - { img: all_displays.png, alt: servicios, h2: '¡<b>Sumá</b> pantallas!', h3: 'Disfrutá desde la <b class="text-orange2">PC</b>, el <b class="text-orange2">celular</b>, <br class="d-none d-md-block">la <b class="text-orange2">tablet</b> o tu <b class="text-primary">TV</b>.' }
packs:
    - { destacada: destacada-bg-blue, img: hbo-mascotas2.jpg, alt: 'DTV Pack HBO', title: '<b>¡Lo mejor de HBO!</b>', p: 'Series originales, estrenos exclusivos, documentales únicos y más.', cta: '<b>Disfrutalo de cortesía por 6 meses.</b>' }
    - { img: foxpremium-rabbit.jpg, alt: 'DTV Pack FOX PREMIUM', title: '<b>¡Más series y más películas!</b>', p: 'Disfrutá nuevos capítulos, vive lanzamientos recientes de cine y eventos deportivos estelares.' }
---
